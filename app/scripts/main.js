$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('#fullpage').fullpage({
    //options here
    anchors: [
      'section_1',
      'section_2',
      'section_3',
      'section_4',
      'section_5',
      'section_6',
      'section_7',
      'section_8',
      'section_9',
      'section_10',
    ],
    scrollBar: true,
    autoScrolling: false,
    scrollOverflowReset: true,
    /* scrollOverflow: true, */
    scrollHorizontally: false,
    responsiveWidth: 1199,
  });

  $(window).on('load resize', function () {
    if ($(window).width() < 768) {
      fullpage_api.setAllowScrolling(false);
    }
  });

  $('a[href="#section_10/slide_10_2"]').on('click', function (e) {
    $('html, body').animate(
      {
        scrollTop: $('div[data-anchor="slide_10_2"] .l-quiz__title').offset()
          .top,
      },
      500
    );
  });

  $('a[href="#section_10/slide_10_5"]').on('click', function (e) {
    $('html, body').animate(
      {
        scrollTop: $('div[data-anchor="slide_10_5"] .l-quiz__title').offset()
          .top,
      },
      500
    );
  });

  let stats_date, stats_people;

  $('input[name=\'date\']').on('input', function () {
    stats_date = $(this).val();
    $('.stats_date').html(stats_date);
    fullpage_api.moveTo('section_10', 1);

    $('html, body').animate(
      {
        scrollTop: $('div[data-anchor="slide_10_3"] .l-quiz__title').offset()
          .top,
      },
      500
    );
  });

  $('input[name=\'people\']').on('input', function () {
    stats_people = $(this).val();
    $('.stats_people').html(stats_people);
    $('#quiz_textarea').val(`Ждём на профосмотр:
    • ${stats_date};
    • ${stats_people}
    `);
    fullpage_api.moveTo('section_10', 2);

    $('html, body').animate(
      {
        scrollTop: $('div[data-anchor="slide_10_4"] .l-quiz__title').offset()
          .top,
      },
      500
    );
  });
});
